/*
 * slush-maximoplus-f7
 * https://github.com/dusan/slush-maximoplus-f7
 *
 * Copyright (c) 2016, Dusan Miloradovic
 * Licensed under the MIT license.
 */

'use strict';

var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    install = require('gulp-install'),
    conflict = require('gulp-conflict'),
    template = require('gulp-template'),
    rename = require('gulp-rename'),
    _ = require('underscore.string'),
    inquirer = require('inquirer'),
    path = require('path'),
    argv = require('yargs').argv,
    gutil = require('gulp-util'),
    linedelete=require('gulp-delete-lines'),
    stringreplace=require('gulp-string-replace');


function format(string) {
  var username = string.toLowerCase();
  return username.replace(/\s/g, '');
}

var defaults = (function () {
                  var workingDirName = path.basename(process.cwd()),
                      homeDir, osUserName, configFile, user;

                  if (process.platform === 'win32') {
                    homeDir = process.env.USERPROFILE;
                    osUserName = process.env.USERNAME || path.basename(homeDir).toLowerCase();
                  }
                  else {
                    homeDir = process.env.HOME || process.env.HOMEPATH;
                    osUserName = homeDir && homeDir.split('/').pop() || 'root';
                  }

                  configFile = path.join(homeDir, '.gitconfig');
                  user = {};

                  if (require('fs').existsSync(configFile)) {
                    user = require('iniparser').parseSync(configFile).user;
                  }

                  return {
                    appName: workingDirName,
                    userName: osUserName || format(user.name || ''),
                    authorName: user.name || '',
                    authorEmail: user.email || ''
                  };
                })();

gulp.task('default', function (done) {
  var prompts = [{
    name: 'appName',
    message: 'What is the name of your project?',
    default: defaults.appName
  }, {
    name: 'appDescription',
    message: 'What is the description?'
  }, {
    name: 'appVersion',
    message: 'What is the version of your project?',
    default: '0.1.0'
  }, {
    name: 'authorName',
    message: 'What is the author name?',
    default: defaults.authorName
  }, {
    name: 'authorEmail',
    message: 'What is the author email?',
    default: defaults.authorEmail
  }, {
    name: 'userName',
    message: 'What is the github username?',
    default: defaults.userName
  }, {
    type: 'confirm',
    name: 'moveon',
    message: 'Continue?'
  }];
  
  var cacheDir=argv.cacheDir;
  var offline=argv.offline;

  var installOptions=offline?{npm:"--cache-min 999999", bower:"--offline"}:{};

  
  function replaceRc(file){
    return (cacheDir!=null) && ((".bowerrc"==file.relative) ||  (".npmrc"==file.relative));
  }

  function deleteFromRc(file){
    return (cacheDir==null) && ((".bowerrc"==file.relative) ||  (".npmrc"==file.relative));
  }

  //Ask
  inquirer.prompt(prompts,
                  function (answers) {
                    if (!answers.moveon) {
                      return done();
                    }
                    answers.appNameSlug = _.slugify(answers.appName);
                    gulp.src(__dirname + '/templates/**',{dot:true})
                    .pipe(gulpif(/[.]json$/,template(answers)))
                    .pipe(rename(function (file) {
                            if (file.basename[0] === '_') {
                              file.basename = '.' + file.basename.slice(1);
                            }
                          }))
                    .pipe(gulpif(replaceRc,stringreplace("@@cache@@",cacheDir)))
                    .pipe(gulpif(deleteFromRc,linedelete({filters:[new RegExp("@@cache@@")]})))
                    .pipe(gulp.dest('./'))
                    .pipe(install(installOptions))
                    .on('end', function () {
                      done();
                    });
                  });
});

gulp.task('gui',function(done){
  var answerKeys = argv.answerKeys.split(",");
  var answerValues = argv.answerValues.split(",");
  var answers={};
  var cacheDir=argv.cacheDir;
  var offline=argv.offline;

  var installOptions=offline?{npm:"--cache-min 999999", bower:"--offline"}:{};

  
  function replaceRc(file){
    return (cacheDir!=null) && ((".bowerrc"==file.relative) ||  (".npmrc"==file.relative));
  }

  function deleteFromRc(file){
    return (cacheDir==null) && ((".bowerrc"==file.relative) ||  (".npmrc"==file.relative));
  }

  for (var j=0;j<answerKeys.length;j++){
    answers[answerKeys[j]] = answerValues[j];
  }
  console.log('gui '+JSON.stringify(answers));
  answers.appNameSlug = _.slugify(answers.appName);
  gulp.src(__dirname + '/templates/**',{dot:true})
  .pipe(gulpif(/[.]json$/,template(answers)))
  .pipe(rename(function (file) {
          if (file.basename[0] === '_') {
            file.basename = '.' + file.basename.slice(1);
          }
        }))
  .pipe(gulpif(replaceRc,stringreplace("@@cache@@",cacheDir)))
  .pipe(gulpif(deleteFromRc,linedelete({filters:[new RegExp("@@cache@@")]})))
  //  .pipe(conflict('./'))
  .pipe(gulp.dest('./'))
  .pipe(install(installOptions))
  .on('end', function () {
    done();
  });
  
});


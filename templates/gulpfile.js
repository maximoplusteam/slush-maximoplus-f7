var gulp=require('gulp');
var watch=require('gulp-watch');
var replace=require('gulp-replace');
var inline = require('gulp-inline');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var base64 = require('gulp-inline-base64');

gulp.task('cordovainit',function(){
    gulp.src(['src/**/*'])
	.pipe(replace('../bower_components','bower_components'))
	.pipe(replace('<!--script type="text/javascript" src="cordova.js"></script-->','<script type="text/javascript" src="cordova.js"></script>'))
	.pipe(gulp.dest('./.cordovaapp/www'));
});

gulp.task('cordovacopy',function(){
    watch(['src/**/*'])
    	.pipe(replace('../bower_components','bower_components'))
	.pipe(replace('<!--script type="text/javascript" src="cordova.js"></script-->','<script type="text/javascript" src="cordova.js"></script>'))
	.pipe(gulp.dest('./.cordovaapp/www'));
});

gulp.task('prod',function(){
    gulp.src('src/index.html')
    .pipe(inline({
    base: 'src/',
    js: function() {
      return uglify({
          mangle: false
      })}
      ,
      css:[function(){
             return base64({
               baseDir:'src/'
             })
           },minifyCss]
  }))
  .pipe(gulp.dest('.cordovaprod/www/'));
});